# Gmarkov
This is the CLI interface to go with my Markov chain library called [gmarkov-lib](https://crates.io/crates/gmarkov-lib).

The program takes one filename as input for the Markov chain, `-` can be used to
read stdin to feed the Markov chain. 

The program creates a markov chain with an order specified by the `-o` or
`--order` options defaults to 1

The program outputs lines of text based on the trained markov chain. The `-n`
or `--lines` options can be used to specify the number of lines to output
